#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

static void merge(int* array, int p, int q, int r)
{
	int i,k;
	int begin1,end1,begin2,end2;
	int* temp = (int*)malloc((r - p + 1)*sizeof(int));
	begin1 = p;
	end1   = q;
	begin2 = q + 1;
	end2   = r;
	k = 0;
	while ((begin1 <= end1) && ( begin2 <= end2)) {
		if(array[begin1] < array[begin2]) {
			temp[k] = array[begin1];
			begin1++;
		}
		else {
			temp[k] = array[begin2];
			begin2++;
		}
		k++;
	}
	while (begin1 <= end1 || begin2 <= end2) {
		if (begin1 <= end1)
			temp[k++] = array[begin1++];
		if (begin2 <= end2)
			temp[k++] = array[begin2++];
	}
	for (i = 0; i <= (r - p); i++)
		array[p+i] = temp[i];
	free(temp);
}

struct param{
	void *base;
	size_t nmemb;
	size_t size;
	int(*compar)(const void *, const void *);
};

static void *_sort_main(void *param)
{
	struct param *p = (struct param*)param;

	qsort(p->base, p->nmemb, p->size, p->compar);

	free(param);
	pthread_exit(0);
	return NULL;
}

static pthread_t pthread_sort(void *base, size_t nmemb, size_t size,
		int(*compar)(const void *, const void *))
{
	pthread_t thread;
	struct param *p = malloc(sizeof(struct param));

	p->base   = base;
	p->nmemb  = nmemb;
	p->size   = size;
	p->compar = compar;

	if (pthread_create(&thread, NULL, _sort_main, p) != 0)
		printf("pthread_create error\n");
	return thread;
}

//#define DEBUG printf
#define DEBUG(args ...)

void qqsort(void *base, size_t nmemb, size_t size,
		int(*compar)(const void *, const void *))
{
	long cpu_num = sysconf(_SC_NPROCESSORS_ONLN);
	pthread_t *p = malloc(sizeof(pthread_t) * cpu_num);
	uint8_t *data = (uint8_t*)base;
	int i, start = 0, mid, per_size = nmemb / cpu_num, end, mod = nmemb % cpu_num;

	DEBUG("cpu num = %ld, per=%d, mod=%d\n", cpu_num, per_size, mod);
	for (i = 0; i < cpu_num; i++) {
		DEBUG("start= %d, end = %d\n", start, start + per_size);
		p[i] = pthread_sort(data + start * size, per_size, size, compar);
		start += per_size;
		// 最后不够除
		if (start + per_size + mod >= nmemb)
			per_size = nmemb - start;
	}

	for (i = 0; i < cpu_num; i++)
		pthread_join(p[i], NULL);

	per_size = nmemb / cpu_num;
	mid = 0;
	for (i = 0; i < cpu_num - 1; i++) {
		mid += per_size;
		end = mid + per_size;
		if (end + mod >= nmemb)
			end = nmemb;
		merge((int*)base, 0, mid - 1 , end - 1);

		DEBUG("%d, %d, %d\n", 0, mid - 1, end - 1);
	}

	free(p);
}

