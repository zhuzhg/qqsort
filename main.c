#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>

#include "qqsort.h"

inline static int compar(const void *a, const void *b)
{
	return *(uint32_t*)a - *(uint32_t*)b;
}

static uint32_t GetTickCount(void)
{
	struct timeval current;
	gettimeofday(&current, NULL);
	return current.tv_sec * 1000 + current.tv_usec/1000;
}

#define NUM 4000 * 10000
int main(int argc, char **argv)
{
	int i = 0, num = NUM;
	uint32_t *data, *q_data, *qq_data ;
	uint32_t start, end, speed1, speed2;

	if (argc > 1)
		num = atoi(argv[1]);

	data    = malloc(num *sizeof(uint32_t));
	q_data  = malloc(num *sizeof(uint32_t));
	qq_data = malloc(num *sizeof(uint32_t));

	data[i] = q_data[i] = qq_data[i] = 0;
	for (i = 0; i < num; i++)
//		data[i] = random() % 100;
		data[i] = random();

#if 1
	speed1 = 0;
	for (i=0; i < 2; i++) {
		memcpy(q_data, data, sizeof(uint32_t) * num);
		start = GetTickCount();
		qsort(q_data, num, sizeof(uint32_t), compar);
		end = GetTickCount();
		speed1 += end - start;
	}
	printf("size: %d , qsort time: %3.2f s.\n", num, speed1 / 1000.0);
#endif

#if 1
	speed2 = 0;
	for (i=0; i < 2; i++) {
		memcpy(qq_data, data, sizeof(uint32_t) * num);
		start = GetTickCount();
		qqsort(qq_data, num, sizeof(uint32_t), compar);
		end = GetTickCount();
		speed2 += end - start;
	}
	printf("size: %d , qqsort time: %3.2f s.\n", num, speed2 / 1000.0);
#endif
	for (i = 0; i < num; i++)
		if (q_data[i] != qq_data[i]) {
			printf("check error[%d]: %d -> %d\n", i, q_data[i], qq_data[i]);
			break;
		}

	if (speed2 < speed1)
		printf("check ok, fast: %3.2f %%.\n", (speed1 - speed2) / (speed1 / 100.0));
	else
		printf("check ok, slow: %3.2f %%.\n", (speed2 - speed1) / (speed1 / 100.0));

	return 0;
}

