#ifndef QQSORT_H
#define QQSORT_H

void qqsort(void *base, size_t nmemb, size_t size,
		int(*compar)(const void *, const void *));

#endif
